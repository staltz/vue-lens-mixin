module.exports = {
  props: ['lens'],
  computed: {
    state: {
      get: function() {
        const parent = this.$parent.state;
        const child = this.lens.get(parent);
        return child;
      },
      set: function(newChild) {
        const oldParent = this.$parent.state;
        const newParent = this.lens.set(newChild, oldParent);
        this.$parent.state = newParent;
        this.$parent.$forceUpdate();
      },
    },
  },
};
