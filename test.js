var test = require('tape');
var lensMixin = require('./index');

test('has the stuff', function(t) {
  t.plan(3);
  t.deepEquals(lensMixin.props, ['lens']);
  t.ok(lensMixin.computed.state.get);
  t.ok(lensMixin.computed.state.set);
  t.end();
});
